module.exports = function() {
	$.gulp.task("watch", function() {
		return new Promise((res, rej) => {
			$.gp.watch("./app/**/*.html", $.gulp.series("html"));
			$.gp.watch("./app/js/**/*.js", $.gulp.series("scripts"));
			$.gp.watch("./app/scss/**/*.scss", $.gulp.series("styles"));
			res();
		});
	});
};