module.exports = function () {
	$.gulp.task('bowerJs', function() {
		if ( $.bowerProduction ) {
			return $.gulp.src($.mainFiles('**/*.js'))
				.pipe($.gp.uglify())
				.pipe($.gulp.dest('./app/js/libs/'))
		} else {
			return $.gulp.src($.mainFiles('**/*.js', {
				paths: {
					bowerDirectory: '../bower',
					bowerrc: './.bowerrc',
					bowerJson: './bower.json'
				}
			}))
				.pipe($.gulp.dest('./app/js/libs/'))
		}
	});
};