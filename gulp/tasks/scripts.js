module.exports = function() {
	$.gulp.task("scripts", function() {
		return $.gulp.src("./app/js/**/*")
			// .pipe($.gp.sourcemaps.init())
			.pipe($.gp.newer('./build/js/'))
			// .pipe($.gp.if($.minificate, $.gp.uglify()))
			// .pipe($.gp.sourcemaps.write("./maps/"))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
};