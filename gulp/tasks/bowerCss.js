module.exports = function () {
	$.gulp.task('bowerCss', function() {
		if ( $.bowerProduction ) {
			return $.gulp.src($.mainFiles('**/*.css'))
				.pipe($.gulp.dest('./app/scss/libs'))
		} else {
			return $.gulp.src($.mainFiles('**/*.css', {
				paths: {
					bowerDirectory: '../bower',
					bowerrc: './.bowerrc',
					bowerJson: './bower.json'
				}
			}))
				.pipe($.gulp.dest('./app/scss/libs'))
		}
	});
};