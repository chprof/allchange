module.exports = function () {
	$.gulp.task("cssVendor", function () {
		return $.gulp.src("./app/scss/libs/*.css")
			.pipe($.gp.concat('vendor.css'))
			.pipe($.gp.cleanCss({
				rebase: false
			}))
			.pipe($.gp.rename({ suffix: ".min" }))
			.pipe($.gulp.dest("./build/css/"))
			// .on("end", $.bs.reload);
	});
};