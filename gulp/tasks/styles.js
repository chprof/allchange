module.exports = function () {
	var processors = [
		$.prefix({
			browsers: ['last 10 version'],
			cascade: false
		}),
		$.mqpacker({
			sort: $.sortMedia
		})
	]
	$.gulp.task("styles", function () {
		return $.gulp.src("./app/scss/**/*.scss")
			.pipe($.gp.plumber({
				errorHandler: $.gp.notify.onError(function(err) {
					return {
						title: 'Plumber Error: Styles',
						message: err.message
					}
				})
			}))
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.sass({
				outputStyle: 'expanded',
				errLogToConsole: true,
				includePaths: 'node_modules'
			}).on('error', $.gp.sass.logError))
			.pipe($.gp.postcss(processors))
			.pipe($.gp.plumber.stop())
			.pipe($.gp.sourcemaps.write("./maps/"))
			.pipe($.gp.if($.minificate, $.gp.cleanCss({
				rebase: false
			}) ))
			.pipe($.gulp.dest("./build/css/"))
			.on("end", $.bs.reload);
	});
};