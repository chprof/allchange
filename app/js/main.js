(function() {
	var sliderSelf = document.querySelectorAll('[data-slider="news-slider"]');
	if ( sliderSelf.length ) {
		console.log('lalala')
		var slider = tns({
			container: '[data-slider="news-slider"]',
			items: 3,
			gutter: 30,
			slideBy: '1',
			autoplay: true,
			nav: false,
			autoplayButtonOutput: false,
			prevButton: false,
			controlsText: ['<span><span><img src="images/Triangle.svg" alt=""></span></span>', '<span><span><img src="images/Triangle.svg" alt=""></span></span>'],
			responsive: {
				0: {
					items: 1
				},
				576: {
					items: 2
				},
				768: {
					items: 3
				},
				992: {
					items: 2
				},
				1200: {
					items: 3
				}
			}
		});
	};

	function Tabs(tabSelector) {
		var bindAll = function() {
			var menuElements = document.querySelectorAll('[' + tabSelector + ']');
			for(var i = 0; i < menuElements.length ; i++) {
				menuElements[i].addEventListener('click', change, false);
			}
		};
		var clear = function() {
			var menuElements = document.querySelectorAll('[' + tabSelector + ']');
			for(var i = 0; i < menuElements.length ; i++) {
				menuElements[i].classList.remove('active');
				var id = menuElements[i].getAttribute(tabSelector);
				document.getElementById(id).classList.remove('active');
			}
		};
		var change = function(e) {
			clear();
			e.preventDefault();
			e.target.classList.add('active');
			var id = e.currentTarget.getAttribute(tabSelector);
			document.getElementById(id).classList.add('active');
		}
		bindAll();
	};

	function Collapse() {
		function BindAll() {
			var collapseBtn = document.querySelectorAll('[data-toggle="collapse"]');
			for( var i = 0; i < collapseBtn.length; i++ ) {
				collapseBtn[i].addEventListener('click', change, false);
			}
		};
		function change(e) {
			e.preventDefault();
			if ( e.currentTarget.hasAttribute('data-target') ) {
				e.target.classList.toggle('collapsed');
				var id = e.currentTarget.getAttribute('data-target');
				document.getElementById(id).classList.toggle('show');
				if ( this.hasAttribute('data-text-changed') ) {
					var mesAfterCollapse = this.getAttribute('data-text-changed');
					var mesBeforeCollapse = this.innerHTML;
					this.textContent = mesAfterCollapse;
					this.setAttribute('data-text-changed', mesBeforeCollapse);
				}
			};
			if ( e.currentTarget.classList.contains('link_review_js') ) {
				this.style.display = 'none';
			};
			if ( e.currentTarget.classList.contains('link_review_hide') ) {
				document.querySelector('.link_review_js').style.display = 'block';
			};
		};
		BindAll();
	};

	function MenuMobileToggle() {
		function BindAll() {
			var openBtn = document.querySelectorAll('[data-open]');
			var closeBtn = document.querySelectorAll('[data-close]');
			for( var i = 0; i < openBtn.length; i++ ) {
				openBtn[i].addEventListener('click', openMenu, false);
			}
			for( var i = 0; i < closeBtn.length; i++ ) {
				closeBtn[i].addEventListener('click', closeMenu, false);
			}
		}
		function openMenu(e) {
			var id = e.currentTarget.getAttribute('data-open');
			document.getElementById(id).classList.add('visible');
		};
		function closeMenu(e) {
			var id = e.currentTarget.getAttribute('data-close');
			document.getElementById(id).classList.remove('visible');
		};
		BindAll();
	};

	function SetDisableLinks() {
		var links = document.querySelectorAll('.sorted-links__item');
		for (var i = 0; i < links.length; i++) {
			links[i].addEventListener('click', function() {
				if (this.classList.contains('disabled')) {
					event.preventDefault();
				}
			});
		}
	};
	
	function SetLocationForTableLinks(tr, link) {
		var mainTableTr = document.querySelectorAll(tr);
		for (var i = 0; i < mainTableTr.length; i++) {
			mainTableTr[i].addEventListener('click', function(e){
				var element = this.querySelector(link);
				var href = element.href;
				window.open(href, '_blank');
			})
		}
	};

	function SetStopPropagation(link) {
		var lastCellLink = document.querySelectorAll(link);
		for (var k = 0; k < lastCellLink.length; k++) {
			lastCellLink[k].addEventListener('click', function(e){
				e.stopPropagation();
			})
		}
	};
	function SetSelect() {
		var selects = document.querySelectorAll('.choices-select');
		var strArr = [];
		for(var i = 0; i < selects.length; i++ ) {
			var selectsClass = selects[i].classList;
			var str = '';
			for ( var k = 0; k < selectsClass.length; k++ ) {
				str = str + selectsClass[k] + ' ';
			}
			var choices = new Choices(selects[i], {
				classNames: {
					containerOuter: 'choices ' + str
				}
			});
		};
	};

	function setToggle() {
		var elem = document.querySelectorAll('[toggle-active]');
		for (var i = 0; i < elem.length; i++) {
			elem[i].addEventListener('click', function() {
				this.classList.toggle('active');
			})
		}
		// elem.addEventListener('click', function() {
		// 	console.log(this)
		// })
	}

	svg4everybody({loadSprite:!1});
	var navTabs = new Tabs('data-tab');
	var contentTabs = new Tabs('data-tab-content');
	var collapse = new Collapse();
	var menusToggle = new MenuMobileToggle();
	var setLocation = new SetLocationForTableLinks('.main-table tbody tr', '.main-table__exchange-name_js');
	// var setLocationPopular = new SetLocationForTableLinks('.popular-statistic tr', '.popular-statistic__exchange');
	var stopProp = new SetStopPropagation('.main-table tr td:last-child a');
	var setAsideDisableLinks = new SetDisableLinks();
	var select = new SetSelect();
	var disclosure = new Houdini('[data-houdini-group="faq"]', {
		isAccordion: true,
		collapseOthers: true
	});
	var disclosureButton = new Houdini('[data-houdini]');
	setToggle();
}())